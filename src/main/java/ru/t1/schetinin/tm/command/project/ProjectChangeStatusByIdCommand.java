package ru.t1.schetinin.tm.command.project;

import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-change-status-by-id";

    private static final String DESCRIPTION = "Change project status by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }

}
