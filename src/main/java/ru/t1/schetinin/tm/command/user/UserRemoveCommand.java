package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    private static final String NAME = "user-remove";

    private static final String DESCRIPTION = "User remove.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

}