package ru.t1.schetinin.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private static final String NAME = "task-clear";

    private static final String DESCRIPTION = "Delete all tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear(userId);
    }

}