package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "update-user-profile";

    private static final String DESCRIPTION = "Update profile of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("LAST NAME");
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}