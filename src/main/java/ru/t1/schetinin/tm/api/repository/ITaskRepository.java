package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

}
