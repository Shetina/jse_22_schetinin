package ru.t1.schetinin.tm.api.service;

import ru.t1.schetinin.tm.api.repository.IUserOwnedRepository;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}