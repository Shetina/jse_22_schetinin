package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
